# README #

This is a test for developer candidates willing to work in Transparent.

### What is this repository for? ###

* You relax and code.
* We test your problem-solving mental processes.
* We test your coding style.
* You to test if you might like this job.
* You help us improve this test.

### How do I get set up? ###

* `npm i`
* `npm run lint`
* `npm run test exercise-<numExercise>`

### What do I have to do? ###

Goal: Modify the function exported in analysis.js file to make `npm run lint` and `npm run test exercise-<numExercise>` commands pass and output no errors.

The evaluation will not be based in the ability of the code to pass the tests. The tests are only a way to determine if the solution might be correct, and to help the developer to clarify what is expected in the solution, but will not contain all the cases that the solution might be exposed.

### Environment

There is a module defined in lib.js that tries to simulate an access to the database based on documents. It has 2 promise based methods, `insertMany` to insert documents, and `getNext` to interate the collection document by document.

The documents stored try to emulate simple listings (hotel rooms, apartments, houses, etc). They have string ids in `listingId` and other fields like `ownerId`. Listings might be related between each other defined in the field `multilistings` that contain an array of other `listingId`. You can assume that if listing `A` has `multilisting` relation to `B`, then listing `B` will exist and will have `multilisting` relation to `A`, so it works in both directions:

```
{ listingId: 'A', multilistings: ['B'] },
{ listingId: 'B', multilistings: ['A'] },
```

These relations are bidirectional and represent real listings that are duplicated because they are published multiple times in the websites.

There are multiple exercises, one in each directory exercise-<exerciseNumber>

Each exercise contains a test to run in `mocha` that validates the solution, and the analysis that is a function that you have to define.

In all the exercises, you have the following constraints:

- You must use the function `getNext` to iterate over the collection of listings
- You have to define a code that passes the linter validation

### Exercise 1

Nothing to do here for now

### Exercise 2

Let's assume that if 2 listings are crosslisted (they have a multilisting relation), then the owners of both listings represent the same real individual, even if the owner id is different. So one real individual might have multiple owner ids. The goal is to find all the unique individuals in the collection and report some basic aggregated metrics about them. The function is supposed to return a promise that resolves to an array of objects like `{ ownerIds, numListings, numOwners }`, being: `ownerIds` an array of strings, `numListings` an integer and `numOwners` an integer. Those objects represent groups of owners that target to the same individual. 

The listings have this shape:
```
{
  listingId: '123',
  ownerId: 'AB731',
  multilistings: ['111'],
}
```

### How do I delivery the solution? ###
- Fork this repository into a private repository in your account
- Commit the solution
- Add Nil (username: lienbcn) as a collaborator into the repository

### Who do I talk to? ###

In case any clarification is needed do not hesitate to contact with [Nil](nil@seetransparent.com)!

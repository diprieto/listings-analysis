
const _ = require('lodash');
const lib = require('./lib');
const exercise2Analysis = require('./exercise-2/analysis');

// Define some base listings
const someListings = _.range(1, 10e3 + 1).map((i) => {
  return {
    listingId: _.toString(i),
    ownerId: `OWNER${_.toString(_.random(1, 20))}`,
    multilistings: [],
  };
});

// Add random multilisting relations
_.each(someListings, (listing) => {
  // Define the number of listings to add the relation in multilistings:
  const rand = Math.random();
  let multilistingsSize = 0;
  if (rand < 0.002) {
    multilistingsSize = 1;
  }
  // Get some random listings, but avoid selecting the same listing:
  const sampleListings = _.filter(
    _.sampleSize(someListings, multilistingsSize),
    l => l !== listing,
  );
  // For each random listing selected, add the multilistings relation in both sides:
  _.each(sampleListings, (sampleListing) => {
    listing.multilistings.push(sampleListing.listingId);
    sampleListing.multilistings.push(listing.listingId);
    sampleListing.multilistings = _.uniq(sampleListing.multilistings);
  });
  listing.multilistings = _.uniq(listing.multilistings);
});

lib.insertMany(someListings)
  .then(() => exercise2Analysis())
  .then((result) => {
    console.log(`Owner groups found: \n${JSON.stringify(result, null, '  ')}`);
  })
  .catch(console.error);

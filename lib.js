const _ = require('lodash');
const Bluebird = require('bluebird');

let collection = [];

function insertMany(listings) {
  const newCollection = _.concat(collection, listings);
  // All elements have listing id and is not repeated
  if (_.size(_.uniqBy(newCollection, 'listingId')) !== _.size(newCollection)) {
    return Bluebird.reject(new Error('Duplicated listing ids'));
  }
  if (_.find(newCollection, listing => !_.isString(_.get(listing, 'listingId')))) {
    return Bluebird.reject(new Error('Invalid listing ids'));
  }
  collection = newCollection;
  return Bluebird.resolve();
}

/**
 * Get next listing based on previous listing id.
 * Returns null if it reached end of collection.
 * listingId is the Listing id of the previous document.
 * Use null to return the first document.
 * @param {String} previousId
 * @returns {Bluebird<Listing>}
 */
function getNext(previousId) {
  const previousIndex = _.findIndex(collection, { listingId: previousId });
  if (previousIndex === -1) {
    return Bluebird.resolve(_.get(collection, [0]) || null);
  }
  const nextListing = _.get(collection, [previousIndex + 1]) || null;
  return Bluebird.resolve(nextListing);
}

function getCollection() {
  return collection;
}

function reset() {
  collection = [];
}

module.exports = _.assign(
  {
    insertMany,
    getNext,
  },
  (process.env.NODE_ENV === 'test' ? { getCollection, reset } : {}),
);

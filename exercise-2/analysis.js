const lib = require('../lib'); // eslint-disable-line

/**
 *  Analyze and process the new items
 * @param listing My Custom Structure
 * @param newItem
 * @returns {*}
 */
function analyze(listing, newItem) {
  let done = false;

  newItem.multilistings.push(newItem.listingId);

  for (let i = 0; i < listing.length; i += 1) {
    if (
      listing[i].listingIds.some(r => newItem.multilistings.includes(r)) ||
      listing[i].ownerIds.includes(newItem.ownerId)
    ) {
      listing[i].ownerIds.push(newItem.ownerId);
      listing[i].listingIds = listing[i].listingIds.concat(newItem.multilistings);
      // Remove duplicates
      listing[i].listingIds = [...new Set(listing[i].listingIds)];
      listing[i].ownerIds = [...new Set(listing[i].ownerIds)];
      done = true;
      break;
    }
  }

  if (!done) {
    listing.push({
      ownerIds: [newItem.ownerId],
      listingIds: newItem.multilistings,
    });
  }

  return listing;
}

/**
 * While getNext iterate over the data building my own structure
 * @param listing My Custom Structure
 * @param preId Previous ListingId or null
 * @returns {Promise with the analysed data}
 */
function iterate(listing, preId) {
  return new Promise((resolve, reject) => {
    lib.getNext(preId).then((item) => {
      if (item) {
        iterate(analyze(listing, item), item.listingId).then(resolve, reject);
      } else {
        resolve(listing);
      }
    }, reject);
  });
}

/**
 * Transform my Custom Structure to the required structure
 * @param data my Custom Structure
 * @returns {Array of data like { ownerIds, numListings, numOwners }}
 */
function transform(data) {
  const result = [];

  data.forEach((item) => {
    result.push({
      ownerIds: item.ownerIds,
      numListings: item.listingIds.length,
      numOwners: item.ownerIds.length,
    });
  });

  return result;
}

module.exports = () => {
  return new Promise((resolve, reject) => {
    iterate([]).then((data) => {
      resolve(transform(data));
    }, reject);
  });
};

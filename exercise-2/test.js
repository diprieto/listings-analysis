/* eslint-env node, mocha */

process.env.NODE_ENV = 'test';

const _ = require('lodash');
const runAnalysis = require('./analysis');
const lib = require('../lib');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');

const { assert } = chai;
chai.use(chaiAsPromised);

describe('Exercise 2 - owner group analysis', () => {
  beforeEach(() => {
    lib.reset();
  });
  afterEach(() => {
    lib.reset();
  });
  it('should match all owners that have common listings', () => {
    return lib.insertMany([
      { listingId: '1', ownerId: 'ALICE', multilistings: ['2'] },
      { listingId: '2', ownerId: 'BOB', multilistings: ['1', '3'] },
      { listingId: '3', ownerId: 'CHARLIE', multilistings: ['2'] },
    ])
      .then(() => runAnalysis())
      .then((result) => {
        assert.lengthOf(result, 1, 'Only one owner group');
        const { ownerIds } = result[0];
        assert.sameMembers(ownerIds, ['ALICE', 'BOB', 'CHARLIE']);
      });
  });
  it('should not match 2 owners that do not have common listings', () => {
    return lib.insertMany([
      { listingId: '1', ownerId: 'ALICE', multilistings: ['2'] },
      { listingId: '2', ownerId: 'BOB', multilistings: ['1'] },
      { listingId: '3', ownerId: 'CHARLIE', multilistings: [] },
    ])
      .then(() => runAnalysis())
      .then((result) => {
        assert.lengthOf(result, 2, 'Two owner groups');
        const allGroupsOwnerIds = _.map(result, 'ownerIds');
        assert.sameMembers(allGroupsOwnerIds[0], ['ALICE', 'BOB']);
        assert.sameMembers(allGroupsOwnerIds[1], ['CHARLIE']);
      });
  });
  it('should group by owner id', () => {
    return lib.insertMany([
      { listingId: '1', ownerId: 'CHARLIE', multilistings: ['2'] },
      { listingId: '2', ownerId: 'CHARLIE', multilistings: ['1'] },
      { listingId: '3', ownerId: 'CHARLIE', multilistings: [] },
    ])
      .then(() => runAnalysis())
      .then((result) => {
        assert.lengthOf(result, 1, 'Only one owner group');
        const { ownerIds } = result[0];
        assert.sameMembers(ownerIds, ['CHARLIE']);
      });
  });
  it('should count number of listings by owner group', () => {
    return lib.insertMany([
      { listingId: '1', ownerId: 'ALICE', multilistings: ['2'] },
      { listingId: '2', ownerId: 'BOB', multilistings: ['1', '3'] },
      { listingId: '3', ownerId: 'CHARLIE', multilistings: ['2'] },
      { listingId: '4', ownerId: 'DIANA', multilistings: [] },
    ])
      .then(() => runAnalysis())
      .then((result) => {
        assert.lengthOf(result, 2);
        const aNumListings = _.map(result, 'numListings');
        assert.sameMembers(aNumListings, [3, 1]);
      });
  });
  it('should count number of owners by owner group', () => {
    return lib.insertMany([
      { listingId: '1', ownerId: 'ALICE', multilistings: ['2'] },
      { listingId: '2', ownerId: 'ALICE', multilistings: ['1', '3'] },
      { listingId: '3', ownerId: 'CHARLIE', multilistings: ['2'] },
      { listingId: '4', ownerId: 'DIANA', multilistings: [] },
    ])
      .then(() => runAnalysis())
      .then((result) => {
        assert.lengthOf(result, 2);
        const aNumOwners = _.map(result, 'numOwners');
        assert.sameMembers(aNumOwners, [2, 1]);
      });
  });
});

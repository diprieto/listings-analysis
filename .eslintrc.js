module.exports = {
  "extends": "airbnb-base",
  rules: {
    'arrow-body-style': 0,
    'no-console': 0,
    'no-param-reassign': 0,
  }
};
/* eslint-env node, mocha */

process.env.NODE_ENV = 'test';

const lib = require('./lib');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');

const { assert } = chai;
chai.use(chaiAsPromised);

describe('lib', () => {
  describe('insertMany', () => {
    beforeEach(() => {
      lib.reset();
    });
    afterEach(() => {
      lib.reset();
    });
    it('should insert listings to the collection', () => {
      return lib.insertMany([{ listingId: '1' }, { listingId: '2' }])
        .then(() => {
          const collection = lib.getCollection();
          assert.deepEqual(collection, [{ listingId: '1' }, { listingId: '2' }]);
          return lib.insertMany([{ listingId: '3' }, { listingId: '4' }]);
        })
        .then(() => {
          const collection = lib.getCollection();
          assert.deepEqual(collection, [{ listingId: '1' }, { listingId: '2' }, { listingId: '3' }, { listingId: '4' }]);
        });
    });
    it('should prevent inserting invalid ids', () => {
      const promiseResult = lib.insertMany([{}]);
      return assert.isRejected(promiseResult, '', 'Rejection expected');
    });
    it('should prevent inserting duplicated ids', () => {
      let promiseResult = lib.insertMany([{ listingId: '1' }]);
      return promiseResult
        .then(() => {
          promiseResult = lib.insertMany([{ listingId: '1' }]);
          return assert.isRejected(promiseResult, '', 'Rejection expected');
        });
    });
  });

  describe('getNext', () => {
    beforeEach(() => {
      lib.reset();
    });
    afterEach(() => {
      lib.reset();
    });
    it('should get first element if null provided', () => {
      return lib.insertMany([{ listingId: '1' }, { listingId: '2' }])
        .then(() => lib.getNext(null))
        .then((first) => {
          assert.deepEqual(first, { listingId: '1' });
        });
    });
    it('should get null if last provided', () => {
      return lib.insertMany([{ listingId: '1' }, { listingId: '2' }])
        .then(() => lib.getNext('2'))
        .then((next) => {
          assert.isNull(next);
        });
    });
    it('should get next properly', () => {
      return lib.insertMany([{ listingId: '1' }, { listingId: '2' }])
        .then(() => lib.getNext('1'))
        .then((next) => {
          assert.deepEqual(next, { listingId: '2' });
        });
    });
  });
});
